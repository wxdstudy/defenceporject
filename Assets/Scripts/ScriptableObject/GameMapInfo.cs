using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 게임 맵 정보를 나태내기 위한 클래스 입니다.
/// </summary>

[CreateAssetMenu(fileName = "GameMapInfo", menuName ="ScriptableObject/GameMapInfo")]

public class GameMapInfo : ScriptableObject
{
    /// <summary>
    /// 게임 맵 크기 X를 나타냅니다.
    /// </summary>
    public int mapSizeX;

    /// <summary>
    /// 게임 맵 크기 Y를 나타냅니다.
    /// </summary>
    public int mapSizeY;

    /// <summary>
    /// 적이 스폰되는 위치
    /// </summary>
    public Vector2Int enemySpawnPosition;

    /// <summary>
    /// 적이 이동하게 될 목적지
    /// </summary>
    public Vector2Int enemyTargetPosition;
}
