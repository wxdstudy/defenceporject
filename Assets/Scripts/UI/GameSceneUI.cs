using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameSceneUI : MonoBehaviour
{
    
    [Header("터렛1 생성 버튼")]
    public Button m_CreatTurret1;

    /// <summary>
    /// 터렛1 생성 버튼 이벤트
    /// </summary>
    public event UnityAction createTurret1ButtonEvent;

    public void Start()
    {
        m_CreatTurret1.onClick.AddListener(createTurret1ButtonEvent);
    }

}
